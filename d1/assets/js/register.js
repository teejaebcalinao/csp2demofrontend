let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	//validate user inputs 
	if((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length == 11)){
		//check if the databse already has the email
		//fetch is a built-in js function that allows getting of data from another source without the need to refresh a page
		//fetch send data to the URL provided with the following parameters
		//method -> HTTP method
		//header -> what kind of data to send
		//body -> the content of the req.body
		fetch("https://fierce-fortress-57910.herokuapp.com/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		//convert the response to json
		.then(response => response.json())
		//data is the json-covered response
		.then(data => {
			//if data is false, then therea are no duplicates
			//if data is true then there are duplicate
			if(data === false){
				//we fetch the registration route
				fetch("https://fierce-fortress-57910.herokuapp.com/api/users", {
					method: "POST",
					headers: {
						"Content-Type" : "application/json"
					},
					body: JSON.stringify({
						firstname: firstName,
						lastname: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(response => response.json())
				.then(data => {
					//if data is true, registration is successful
					//else, something went wrong
					if (data){
						alert("Registration successful")
					} else {
						alert("Registration went wrong")
					}
				})
			} else {
				alert("Email already exists")
			}
		})
	} else {
		alert("Invalid inputs")
	}
})

