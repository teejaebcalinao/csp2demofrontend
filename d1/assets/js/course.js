//Scenario: when accessing a course, the url is as follows:
//course.html?courseId=5e1645e8124
//in order to get the actual details, we need to get the value after the ?courseId=

//window.location.search returns the query string part of the URL
// console.log(window.location.search)
//?courseId=5f4764b4c300e54c809ee8df ->

//To get the actual courseId, we need to use URL search params to access specific parts of the query
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

// console.log(courseId)
//retrieve the JWT stored in localStorage
let token = localStorage.getItem("token")

//populate the information of a course using fetch
let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

fetch(`https://fierce-fortress-57910.herokuapp.com/api/courses/${courseId}`)
.then(res => { return res.json() })
.then(data => {
	console.log(data)

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	//innerHTML is a property of DOM that specifies the content inside a html tag.
	//<h5>innerHTML</h5>
	enrollContainer.innerHTML = `<button id = "enrollButton" class = "btn btn-block btn-primary"> Enroll </button>`

	//after the enroll button has been added, we add an event so that when the button is clicked, the student is enrolled to a class
	document.querySelector("#enrollButton").addEventListener("click", () => {
		// no need to e.preventDefault, as the button doesn't have any default actions when clicked.
		// fetch the enroll function of the backend and send the necessary data to the backend
		// info needed by fetch: token and courseId
		// url: http://localhost:3000/api/users/enroll
		// Mini exercise: prepare the fetch statement to the backend
		// for the two thens, just console.log the data
		fetch("https://fierce-fortress-57910.herokuapp.com/api/users/enroll", {
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${token}`
				//Token is placed in the authorization headers
			}, 
			body: JSON.stringify ({
				courseId : courseId
			})
		})
		.then(res => { return res.json()})
		.then(data => {
			console.log(data) //this would return only true or false
			if (data) {
				alert("You have enrolled successfully")
				window.location.replace("./courses.html")
			} else {
				alert("Enrollment failed")
			}
		})
	})
})
