let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let cardFooter; //use this to add a button at the bottom of each card to go to a specific course

// console.log(adminUser)
if (adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary">
					Add Course
				</a>
			</div>
		`
}

fetch('https://fierce-fortress-57910.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	console.log(data)
	let courseData;

	if(data.length < 1){
		courseData = "No courses available"
	} else {
		courseData = data.map(course => { //break the array into component elements 
			//console.log(course._id)
			if(adminUser == "false" || !adminUser){
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class = "btn btn-primary text-white btn-block">Go to Course</a>`
			} else {
				//Check if the user is an admin, and if they are, create a button that deletes/archives the course
				cardFooter = `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-danger text-white btn-block"> Archive Course </a>`
			}

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-right">${course.price}</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
				`

			)
		}).join("")
	}

	let container = document.querySelector("#coursesContainer")
	
	container.innerHTML = courseData
})