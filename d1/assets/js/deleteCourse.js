//Exercise
//use the lesson for the enroll to archive the course
//Steps
//1. Get the id from the URL

//To get the actual courseId, we need to use URL search params to access specific parts of the query
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token") //cannot delete without admin logged in
//Q: how would i know if the request is from a user or an admin?

//2. Use the id as a parameter to the fetch and specify the URL
fetch(`https://fierce-fortress-57910.herokuapp.com/api/courses/${courseId}`, {
	method : "DELETE",
	headers : {
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => {return res.json()})
.then(data => {
	if (data) {
		alert ("Course Archived")
	} else {
		alert ("Something went wrong")
	}
})
//3. Process the data as needed
//4. Return to the courses page